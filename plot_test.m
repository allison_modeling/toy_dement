%% Plotting %%

% This file provides a simple plotting file using the parameters values
% randomly sampled from the file "mc_parameters.m"

NMC = 1;

% p = mc_parameters(NMC);
p = p2(:,1);

[output,output_1] = run_DEMENT(p);

%% Biomass %%

figure ('Name','Biomass')
plot(output(:,9),output(:,3),'LineWidth',2)
ylabel('Biomass [mgC cm^{-3}]')
xlabel('Time [d]')
title('Microbial Biomass')

%% Polymer + Monomer %%

figure ('Name','substrate')
A = plot(output(:,9),output(:,1),'LineWidth',2);
hold on
B = plot(output(:,9),output(:,2),'LineWidth',2);
hold off
ylabel('[mgC cm^{-3}]')
xlabel('Time [d]')
title('Substrate')
legend([A B],'Polymer','Monomer')

%% Enzyme cost %%

figure ('Name','Enzyme cost')
plot(output(:,9),output_1(:,6)+output_1(:,7),'LineWidth',2)
% ylabel('Biomass [mgC cm^{-3}]')
xlabel('Time [d]')
title('Enzyme cost')

%% Osmolyte pool %%

figure ('Name','Osmolyte cost')
plot(output(:,9),output_1(:,4)+output_1(:,5),'LineWidth',2)
ylabel('Osmolyte [mgC cm^{-3}]')
xlabel('Time [d]')
title('Osmolyte production')

%% CUE_1 %%

figure ('Name','CUE_1')
plot(output(:,9),output(:,3)./(output(:,3)+output(:,7)),'LineWidth',2)
ylabel('CUE')
xlabel('Time [d]')
title('CUE_1')

%% CUE_2 %%

figure ('Name','CUE_2')
plot(output(:,9),1-((output_1(:,1)+output_1(:,3)+output_1(:,4)+output_1(:,5)+output_1(:,6)+output_1(:,7))./(output_1(:,1)+output_1(:,2))),'LineWidth',2)
ylabel('CUE')
xlabel('Time [d]')
title('CUE_2')

%% Mass Balance %%

figure ('Name','Mass Balance')
plot(output(:,9),output(:,1)+output(:,2)+output(:,3)+output(:,4)+output(:,5)+output(:,6)+output(:,7)+output(:,8))
xlabel('Time [d]')
title('Mass Balance')
