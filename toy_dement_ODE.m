% Toy dement ODE %%

% This is an attempt to write a simple version of DEMENT in the ODE form
% neglecting spatial heterogeneity and diffusion processes in the domain. I
% will only focus on processes assuming at the begining a simple case with
% one strain and once polymer in the system.

function vf_ = toy_dement_ODE(t,x,p,q)

% State variables

C_S     = x(1); % Polymer (substrate) [mgC cm-3]
C_M     = x(2); % Monomer [mgC cm-3]
C_B     = x(3); % Taxa (bacteria or fungi) [mgC cm-3]
C_E     = x(4); % Enzyme [mgC cm-3]
C_G     = x(5); % Transporters
% C_O     = x(6); % Osmolytes
% CO2     = x(7); % CO_2 [mgC cm-3]
% DOC     = x(8); % DOC pool

% Model Parameters

A_0     = 10^p(1);  % Product per enzyme per day for degradation
E_0     = p(2);     % Activation energy
k       = p(3);     % Coefficient controlling water potential sensitivity 
A_U     = 10^p(4);  % Product per enzyme per day for uptake
km      = 10^p(5);  % Half saturation constant for polymer degradation
km_U    = 10^p(6);  % Half saturation constant for monomer uptake
Lo      = p(7);  % Abiotic monomer loss rate
k_L     = p(8);  % Water potential sensitivity of abiotic loss
tau_B   = p(9);  % Linear decay rate of Biomass
beta    = p(10); % Drought tolerance parameter 
beta_ec = 10^p(11); % Cost of enzyme as fraction of biomass
z_ec    = 10^p(12); % Cost of enzyme as fraction of uptake
z_uc    = 10^p(13); % Cost of transporter production
o_co    = 10^p(14); % Cost of osmolyte as fraction of biomass
o_in    = 10^p(15); % Cost of osmolyte as fraction of uptake
tau_E   = p(16); % Linear deactivation rate of enzymes
epsi    = p(17); % Carbon assimilation efficiency
E_U     = p(18);     % Activation energy for uptake

% Model Fix parameters

R       = q(1); % Ideal gas constant
T       = q(2); % Temperature
psi     = q(3); % Water potential
Q       = q(4); % Yes/No vector for enyzme of polymer degradation
I_S     = q(5); % Initial content of polymers
Q_U     = q(6); % Yes/No vector for enyzme of monomer uptake
psi_fc  = q(7); % Field capacity water potential

% Extra functions

v_max   = A_0 * exp(-E_0/(R*T)); % Arrhenius equation for degradation
f_psi   = exp(k*psi);            % Water potential function
v_max_U = A_U * exp(-E_U/(R*T)); % Arrhenius equation for uptake 

% Rates

r_degS = v_max*C_E*Q*C_S*f_psi/(km + C_S); % Rate of Polymer degradation (This represents the rate of monomer formation as well)
r_uptM = C_B*C_G*f_psi*(v_max_U*C_M*Q_U/(km_U + C_M)); % Rate of monomer uptake
r_lecM = C_M*Lo*exp(k_L*psi); % Rate of monomer leaching
r_dead = C_B*tau_B*(1-(1-beta)*(psi-psi_fc)*tau_B); % Rate of bacterial/fungi dead
r_E_co = C_B*C_G*beta_ec; % Rate of constituitive formation of enzymes
r_E_in = r_uptM*z_ec; % Rate of inducible formation of enzymes
r_E_de = C_E*tau_E; % Rate of inactivation of enzymes
r_tras = z_uc*C_B; % Rate of trasnporter production
r_O_co = C_B*o_co; % Rate of osmolyte formation of enzymes
r_O_in = r_uptM*o_in; % Rate of osmolyte formation of enzymes
r_resp = r_uptM*(1-epsi); % Rate of respiration

% ODE system

vf_ = zeros(8,1);

vf_(1) = - r_degS; % Polymer pool
vf_(2) = r_degS - r_uptM - r_lecM; % Monomer pool
vf_(3) = r_uptM - r_resp - r_dead - r_E_co - r_E_in - r_tras - r_O_co - r_O_in; % Biomass (bacteria/fungi) pool
vf_(4) = r_E_co + r_E_in - r_E_de; % Enzyme pool
vf_(5) = r_tras; % Transporters pool
vf_(6) = r_O_co + r_O_in; % Osmolytes pool
vf_(7) = r_resp; % CO2 pool
vf_(8) = r_dead; % DOC pool

end

