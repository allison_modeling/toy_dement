% This file collects the parameter ranges using as a reference Allison 2012
% and Wang and Allison 2019. All the parameters have a uniform
% distribution. 

function [p] = mc_parameters(NMC)

% Define non-fixed parameters %

p(1,:)  = random('unif',4,8,1,NMC);    % Product per enzyme per day
p(2,:)  = random('unif',20,80,1,NMC);  % Activation energy
p(3,:)  = random('unif',0.05,0.1,1,NMC);  % Coefficient controlling water potential sensitivity 
p(4,:)  = random('unif',4,8,1,NMC);  % Product per enzyme per day for monomer uptake
p(5,:)  = random('unif',-6,-5,1,NMC);  % Half saturation constant for polymer degradation
p(6,:)  = random('unif',-7,-6,1,NMC);  % Half saturation constant for monomer uptake
p(7,:)  = random('unif',0.05,0.5,1,NMC);  % Abiotic monomer loss rate
p(8,:)  = random('unif',0.05,0.5,1,NMC);  % Water potential sensitivity of abiotic loss
p(9,:)  = random('unif',0.001,0.005,1,NMC);  % Linear decay rate of Biomass
p(10,:) = random('unif',0,1,1,NMC); % Drought tolerance parameter 
p(11,:) = random('unif',-11.2,-8.2,1,NMC); % Cost of enzyme as fraction of biomass
p(12,:) = random('unif',-7.6,-4.6,1,NMC); % Cost of enzyme as fraction of uptake
p(13,:) = random('unif',-9.5,-9,1,NMC); % Cost of transporter production
p(14,:) = random('unif',-16.1,-13.8,1,NMC); % Cost of osmolyte as fraction of biomass
p(15,:) = random('unif',-4.6,-2.3,1,NMC); % Cost of osmolyte as fraction of uptake
p(16,:) = random('unif',0.02,0.05,1,NMC); % Linear deactivation rate of enzymes
p(17,:) = random('unif',0.2,0.5,1,NMC); % Carbon assimilation efficiency
p(18,:) = random('unif',30,40,1,NMC);  % Activation energy

end
