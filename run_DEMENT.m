%% DEMENT_ode %%

% File use to solve the DEMENT-ode model and generating the necessary
% outputs for plotting.

function [output,output_1] = run_DEMENT(p)

% Define fixed parameters %

q(1) = 0.008314; % Ideal gas constant
q(2) = 20+273;   % Temperature
q(3) = -1.5;     % Water potential
q(4) = 1;        % Yes/No vector for enyzme of polymer degradation
q(5) = 150;      % Initial content of polymers
q(6) = 1;        % Yes/No vector for enyzme of monomer uptake
q(7) = -0.03;    % Field capacity water potential

% Calling Model %

abstol = 1e-9;
reltol = 1e-7;
o_opts = odeset('AbsTol',abstol,'RelTol',reltol,'Events',@stopevent,'NonNegative',1:8); %

c(1) = 150;   % Polymer (substrate) [mgC cm-3]
c(2) = 0;     % Monomer [mgC cm-3]
c(3) = 10;   % Taxa (bacteria or fungi) [mgC cm-3]
c(4) = 1;    % Enzyme [mgC cm-3]
c(5) = 1;    % Transporters
c(6) = 0;     % Osmolytes
c(7) = 0;     % CO_2 [mgC cm-3]
c(8) = 0;     % DOC pool

% Run ODE %

t    = [0 365*10];

try
     warning off
tic
[ty,cu] = ode15s(@toy_dement_ODE,t,c,o_opts,p',q);
 catch ME
     warning off
end
if length(cu) < length(t)
    cu = ones(length(t),length(c))*1e+99;
end

if isreal(cu)==0
    cu = ones(length(t),length(c))*1e+99;    
end

C_S     = cu(:,1); % Polymer (substrate) [mgC cm-3]
C_M     = cu(:,2); % Monomer [mgC cm-3]
C_B     = cu(:,3); % Taxa (bacteria or fungi) [mgC cm-3]
C_E     = cu(:,4); % Enzyme [mgC cm-3]
C_G     = cu(:,5); % Transporters
C_O     = cu(:,6); % Osmolytes
CO2     = cu(:,7); % CO_2 [mgC cm-3]
DOC     = cu(:,8); % DOC pool
time    = ty;

output = horzcat(C_S,C_M,C_B,C_E,C_G,C_O,CO2,DOC,time);

%% Rates

% Model Fix parameters

R       = q(1); % Ideal gas constant
T       = q(2); % Temperature
psi     = q(3); % Water potential
Q       = q(4); % Yes/No vector for enyzme of polymer degradation
I_S     = q(5); % Initial content of polymers
Q_U     = q(6); % Yes/No vector for enyzme of monomer uptake
psi_fc  = q(7); % Field capacity water potential

% Model Parameters

A_0     = 10^p(1);  % Product per enzyme per day
E_0     = p(2);  % Activation energy
k       = p(3);  % Coefficient controlling water potential sensitivity 
A_U     = 10^p(4);  % Product per enzyme per day for uptake
km      = 10^p(5);  % Half saturation constant for polymer degradation
km_U    = 10^p(6);  % Half saturation constant for monomer uptake
Lo      = p(7);  % Abiotic monomer loss rate
k_L     = p(8);  % Water potential sensitivity of abiotic loss
tau_B   = p(9);  % Linear decay rate of Biomass
beta    = p(10); % Drought tolerance parameter 
beta_ec = 10^p(11); % Cost of enzyme as fraction of biomass
z_ec    = 10^p(12); % Cost of enzyme as fraction of uptake
z_uc    = 10^p(13); % Cost of transporter production
o_co    = 10^p(14); % Cost of osmolyte as fraction of biomass
o_in    = 10^p(15); % Cost of osmolyte as fraction of uptake
tau_E   = p(16); % Linear deactivation rate of enzymes
epsi    = p(17); % Carbon assimilation efficiency
E_U     = p(18);     % Activation energy for uptake

% Extra functions

v_max = A_0 * exp(-E_0/(R*T)); % Arrhenius equation
f_psi = exp(k*psi);            % Water potential function
v_max_U = A_U * exp(-E_U/(R*T)); % Arrhenius equation for uptake 

% Rates

r_degS = v_max.*C_E.*Q.*C_S.*C_S.*f_psi./(km + C_S); % Rate of Polymer degradation (This represents the rate of monomer formation as well)
r_uptM = C_B.*C_G.*(v_max_U.*C_M.*Q_U./(km_U + C_M)); % Rate of monomer uptake
r_lecM = C_M.*Lo.*exp(k_L.*psi); % Rate of monomer leaching
r_dead = C_B.*tau_B.*(1-(1-beta)*(psi-psi_fc)*tau_B); % Rate of bacterial/fungi dead
r_E_co = C_B.*C_G.*beta_ec; % Rate of constituitive formation of enzymes
r_E_in = r_uptM.*z_ec; % Rate of inducible formation of enzymes
r_E_de = C_E.*tau_E; % Rate of inactivation of enzymes
r_tras = z_uc.*C_B; % Rate of trasnporter production
r_O_co = C_B.*o_co; % Rate of osmolyte formation of enzymes
r_O_in = r_uptM.*f_psi.*o_in; % Rate of osmolyte formation of enzymes
r_resp = r_uptM.*f_psi.*(1-epsi); % Rate of respiration
r_grow = r_uptM - r_E_co - r_E_in - r_tras - r_O_co - r_O_in; 

output_1 = horzcat(r_resp,r_grow,r_tras,r_O_co,r_O_in,r_E_co,r_E_in);

end