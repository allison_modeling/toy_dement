%% DEMENT_discrete %%

% Attempt to run DEMENT in a time-discrete manner as it is my understanding
% how the real DEMENT runs.

NMC = 1;

q(1) = 0.008314; % Ideal gas constant
q(2) = 20+273;   % Temperature
q(3) = -1.5;     % Water potential
q(4) = 1;        % Yes/No vector for enyzme of polymer degradation
q(5) = 150;      % Initial content of polymers
q(6) = 1;        % Yes/No vector for enyzme of monomer uptake
q(7) = -0.03;    % Field capacity water potential
%%
p = p2(:,3);

%% [output,output_1] = run_DEMENT(p);

    
A_0     = 10^p(1);  % Product per enzyme per day for degradation
E_0     = p(2);     % Activation energy
k       = p(3);     % Coefficient controlling water potential sensitivity 
A_U     = 10^p(4);  % Product per enzyme per day for uptake
km      = 10^p(5);  % Half saturation constant for polymer degradation
km_U    = 10^p(6);  % Half saturation constant for monomer uptake
Lo      = p(7);  % Abiotic monomer loss rate
k_L     = p(8);  % Water potential sensitivity of abiotic loss
tau_B   = p(9);  % Linear decay rate of Biomass
beta    = p(10); % Drought tolerance parameter 
beta_ec = 10^p(11); % Cost of enzyme as fraction of biomass
z_ec    = 10^p(12); % Cost of enzyme as fraction of uptake
z_uc    = 10^p(13); % Cost of transporter production
o_co    = 10^p(14); % Cost of osmolyte as fraction of biomass
o_in    = 10^p(15); % Cost of osmolyte as fraction of uptake
tau_E   = p(16); % Linear deactivation rate of enzymes
epsi    = p(17); % Carbon assimilation efficiency
E_U     = p(18);     % Activation energy for uptake

% Model Fix parameters

R       = q(1); % Ideal gas constant
T       = q(2); % Temperature
psi     = q(3); % Water potential
Q       = q(4); % Yes/No vector for enyzme of polymer degradation
I_S     = q(5); % Initial content of polymers
Q_U     = q(6); % Yes/No vector for enyzme of monomer uptake
psi_fc  = q(7); % Field capacity water potential

t      = 365;

C_S    = zeros(1,t); 
C_M    = zeros(1,t);
C_E    = zeros(1,t); 
C_B    = zeros(1,t); 
C_G    = zeros(1,t); 
C_O    = zeros(1,t); 
C_CO   = zeros(1,t); 
C_D    = zeros(1,t); 
tt     = zeros(1,t);
C_S(1) = 3500;
C_M(1) = 0;
C_E(1) = 1;
C_B(1) = 20;
C_G(1) = 1;
C_O(1) = 0;
C_CO(1)= 0;
C_D(1) = 0;
tt(1)  = 0;

v_max   = A_0 * exp(-E_0/(R*T)); % Arrhenius equation for degradation
f_psi   = exp(k*psi);            % Water potential function
v_max_U = A_U * exp(-E_U/(R*T)); % Arrhenius equation for uptake

for i = 1:t-1
    tt(i+1)  = tt(i)+1;
    C_S(i+1) = C_S(i) - v_max*C_E(i)*Q*C_S(i)*f_psi/(km + C_S(i));
    C_M(i+1) = C_M(i) + v_max*C_E(i)*Q*C_S(i)*f_psi/(km + C_S(i)) - ...
               C_B(i)*C_G(i)*f_psi*(v_max_U*C_M(i)*Q_U/(km_U + C_M(i))) - ...
               C_M(i)*Lo*exp(k_L*psi);
    C_B(i+1) = C_B(i) + C_B(i)*C_G(i)*f_psi*(v_max_U*C_M(i)*Q_U/(km_U + C_M(i))) - ...
               C_B(i)*C_G(i)*f_psi*(v_max_U*C_M(i)*Q_U/(km_U + C_M(i)))*(1-epsi) - ...
               C_B(i)*tau_B*(1-(1-beta)*(psi-psi_fc)*tau_B) - C_B(i)*C_G(i)*beta_ec - ...
               C_B(i)*C_G(i)*f_psi*(v_max_U*C_M(i)*Q_U/(km_U + C_M(i)))*z_ec - z_uc*C_B(i) - ...
               C_B(i)*o_co - C_B(i)*C_G(i)*f_psi*(v_max_U*C_M*Q_U/(km_U + C_M))*o_in;
    C_E(i+1) = C_E(i) + C_B(i)*C_G(i)*beta_ec + C_B(i)*C_G(i)*f_psi*(v_max_U*C_M(i)*Q_U/(km_U + C_M(i)))*z_ec - ...
               C_E(i)*tau_E;
    C_G(i+1) = C_G(i) + z_uc*C_B(i);
    C_O(i+1) = C_O(i) + C_B(i)*o_co + C_B(i)*C_G(i)*f_psi*(v_max_U*C_M(i)*Q_U/(km_U + C_M(i)))*o_in;
    C_CO(i+1)= C_CO(i)+ C_B(i)*C_G(i)*f_psi*(v_max_U*C_M(i)*Q_U/(km_U + C_M(i)))*(1-epsi);
    C_D(i+1) = C_D(i) + C_B(i)*tau_B*(1-(1-beta)*(psi-psi_fc)*tau_B);
end


%%
Balance = C_S - C_M - C_B - C_CO - C_D - C_O; 


%%

figure ('Name','Biomass')
plot(tt,C_B,'LineWidth',2)
ylabel('Biomass [mgC cm^{-3}]')
xlabel('Time [d]')
title('Microbial Biomass')

%%
figure ('Name','Balance')
plot(tt,Balance)

%% Polymer + Monomer %%

figure ('Name','substrate')
A = plot(tt,C_S,'LineWidth',2);
hold on
B = plot(tt,C_M,'LineWidth',2);
hold off
ylabel('[mgC cm^{-3}]')
xlabel('Time [d]')
title('Substrate')
legend([A B],'Polymer','Monomer')
